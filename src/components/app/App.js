import React from "react";
import { Component } from "react";
import Header from "../header";
import ItemList from "../item-list/ItemList";
import PersonDetails from "../person-details/PersonDetails";
import RandomPlanet from "../random-planet";
import "./App.css";

export default class App extends Component {

	state = {
		selectedPerson: 8
	}

	onPersonSelected = (id) => {
		this.setState({
			selectedPerson: id
		});
	};
	render () {
		return (
			<div className="stardb-app">
				<Header/>
				<RandomPlanet/>
				<div className="row mb2">
					<div className="col-md-6">
					<ItemList onItemSelected={this.onPersonSelected} />
					</div>
					<div className="col-md-6">
				<PersonDetails personId={this.state.selectedPerson} />
				</div>
				</div>
			</div>
		);
	};
};