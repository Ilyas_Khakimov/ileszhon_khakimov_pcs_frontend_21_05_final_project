import React from "react";
import { Component } from "react";
import "./Spinner.css";

export default class Spinner extends Component {
	render () {
		return (
			<div className="loadingio-spinner-double-ring-a73a7uo193j"><div className="ldio-sr0z3jac6ut">
				<div></div>
				<div></div>
				<div>
					<div></div>
				</div>
				<div>
					<div></div>
				</div>
				</div>
			</div>
		)
	}
}